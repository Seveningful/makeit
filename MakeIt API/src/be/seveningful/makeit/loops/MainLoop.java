package be.seveningful.makeit.loops;

import org.bukkit.plugin.PluginBase;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import be.seveningful.makeit.Game;
import be.seveningful.makeit.GameUtil;
import be.seveningful.makeit.MakeIt;
import be.seveningful.makeit.exceptions.ExceptionThrower;

public class MainLoop extends BukkitRunnable{

	Game game;
	PluginBase plugin;
	MakeIt miapi;
	public MainLoop() {
		miapi = MakeIt.getAPI();
		game = miapi.getGame();
		plugin = miapi.getGamePlugin();
		runTaskTimer(MakeIt.getAPI()	, 0, 1);
		System.out.println("[MIAPI] Launching main loop");

	}

	@Override
	public void run() {

		if(game.getGameState().getAliveAndConnectedPlayers().size() == 0 && game.getGameState().getAliveAndDisconnectedPlayers().size() == 0) {
			ExceptionThrower.thowException(this, "0 players are alive but no winner ?? Wierd ...", true);
			cancel();
			return;
		}
		
		GameUtil.setMOTD(game.getGameState().getGameProgress().getActualMOTD());
		
		/*
		 else if (game.getGameState().getAliveAndConnectedPlayers().size() == 0 && game.getGameState().getAliveAndDisconnectedPlayers().size() != 0) {
			System.out.println("[MIAPI] No players ALIVE connected ...");
			while (game.getGameState().getAliveAndConnectedPlayers().size() == 0 && game.getGameState().getAliveAndDisconnectedPlayers().size() != 0) {
				try {
					Thread.sleep(1000);
					System.out.println("[MIAPI] No players ALIVE connected ... Awaiting for 1 ALIVE CONNECTED player atleast ...");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}

			System.out.println("[MIAPI] Game has restarted !");
		 */


	}

}

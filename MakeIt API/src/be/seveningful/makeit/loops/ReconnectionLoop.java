package be.seveningful.makeit.loops;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import be.seveningful.makeit.GameUtil;
import be.seveningful.makeit.MakeIt;

public class ReconnectionLoop extends BukkitRunnable {

	UUID id;

	public ReconnectionLoop(UUID id) {
		this.id = id;
		runTaskLaterAsynchronously(MakeIt.getAPI(), 
				MakeIt.getAPI().getGame().getGameState().getConnection().getRejoinSeconds()*20);

	}

	@Override
	public void run() {
		if(Bukkit.getPlayer(id) == null) {
			MakeIt.getAPI().getGame().getGameState().removePlayer(id);
			GameUtil.broadcast(ChatColor.RED + Bukkit.getOfflinePlayer(id).getName() + " a �t� �limin� ");
			MakeIt.getAPI().getGame().getGameState().getConnection().removeReconnectionLoop(id);
			}
	}
	
	public UUID getId() {
		return id;
	}

}

package be.seveningful.makeit.countdown;

import org.bukkit.Bukkit;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import be.seveningful.makeit.MakeIt;
import be.seveningful.makeit.exceptions.ExceptionThrower;

public class CountDown implements CountDownManager{

	int seconds = 0;
	int players = 0;

	public int getPlayers() {
		return players;
	}

	public int getSeconds() {
		return seconds;
	}

	public void setPlayers(int players) {
		this.players = players;
	}

	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

	@Override
	public void run() {
		if ( seconds == 0) {
			ExceptionThrower.thowException(this, "0 seconds is not a correct base value", true);
			return;
		}

		if ( players == 0) {
			ExceptionThrower.thowException(this, "0 players is not a correct base value", true);
			return;
		}

		new BukkitRunnable() {
			int realseconds = seconds;
			int ticks = 20;
			@Override
			public void run() {
				if(!(Bukkit.getOnlinePlayers().size() >= getPlayers())) {
					realseconds = seconds;
					for(Player p : Bukkit.getOnlinePlayers()) {

						p.setLevel(seconds);
						p.setExp(1);

					}
					return;
				}
				if(realseconds == 0 && ticks == 0) {
					for(Player p : Bukkit.getOnlinePlayers()) {

						p.setLevel(0);
						p.setExp(0);
						MakeIt.getAPI().getGame().getGameState().addPlayer(p.getUniqueId());
					}
					onFinish();
					

					cancel();
					return;
				}
				if(ticks == 0 ){
					if( realseconds == 0) return;
					ticks = 20;
					realseconds --;
					
					onSecondUpdate(realseconds);
					
					for(Player p : Bukkit.getOnlinePlayers()) {

						p.setLevel(realseconds);
						p.setExp(1 );

					}
				} else {
					for(Player p : Bukkit.getOnlinePlayers()) {

						p.setLevel(realseconds);
						p.setExp((1F/20F) * (float) ticks );

					}
					ticks --;
				}
			}
		}.runTaskTimer(MakeIt.getAPI(), 0, 1);

	}

	@Override
	public void onFinish() {
		MakeIt.getAPI().getGame().start();
		MakeIt.getAPI().runMainTask();
	}

	@Override
	public void onSecondUpdate(int second) {

		if(second == 10) {
			for(Player p : Bukkit.getOnlinePlayers()) {
				p.playNote(p.getLocation(), Instrument.PIANO, new Note(14 ));
			}
		} else if(second <= 5 && second != 0){
			for(Player p : Bukkit.getOnlinePlayers()) {
				p.playNote(p.getLocation(), Instrument.PIANO, new Note(24 - second));
			}
		} else if (second == 0) {
			for(Player p : Bukkit.getOnlinePlayers()) {
				p.playSound(p.getLocation(), Sound.SUCCESSFUL_HIT, 1, 1);
			}
		}
		
	}


}

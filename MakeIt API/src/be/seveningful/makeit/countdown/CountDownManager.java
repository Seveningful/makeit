package be.seveningful.makeit.countdown;


public interface CountDownManager {
	void onFinish();
	
	void onSecondUpdate(int second);
	
	void run();
}

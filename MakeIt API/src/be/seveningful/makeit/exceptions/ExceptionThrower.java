package be.seveningful.makeit.exceptions;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import be.seveningful.makeit.MakeIt;

public class ExceptionThrower {

	
	
	public static void thowException(Object obj, String message, boolean shutdown) {
		System.out.println("[MakeIt - " + MakeIt.getAPI().getGamePlugin().getName() 
				+ "] "  + message 
				+ " (" + obj.getClass().getName() + ".java)");
		
		if(shutdown) {
			Bukkit.broadcastMessage(ChatColor.RED + "Something went wrong ...");
			Bukkit.broadcastMessage(ChatColor.RED + "Server is going to be down in 10 seconds");
			new BukkitRunnable() {
				
				@Override
				public void run() {

					Bukkit.shutdown();
					
				}
			}.runTaskLater(MakeIt.getAPI(), 20*10);
		}
	}
	
}

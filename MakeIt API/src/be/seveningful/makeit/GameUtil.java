package be.seveningful.makeit;

import java.util.List;

import net.minecraft.server.v1_8_R3.DedicatedServer;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

public class GameUtil {

	private static GameState state = MakeIt.getAPI().getGame().getGameState();
	
	/*
	 * Teleports all players on custom list
	 */
	public static void teleportAll(List<Player> players, Location loc) {
		for (Player p : players) {
			p.teleport(loc);
		}
	}

	/*
	 * Sends effect to all players on custom list
	 */
	public static void effectAll(List<Player> players, PotionEffect effect) {
		for (Player p : players) {
			p.addPotionEffect(effect);
		}
	}
	
	/*
	 * Broadcasts a message with 
	 * Mini-game prefix
	 */
	
	public static void broadcast(String message) {
		Bukkit.broadcastMessage(MakeIt.getAPI().getGame().getGameState().getGameNamePrefix() + " " + message);
	}

	/*
	 * Setting default spectator 
	 */
	public static void setSpectator(Player p) {

		p.setGameMode(GameMode.SPECTATOR);
		p.teleport(state.getLocations().getWaitingroom() != null ? state.getLocations().getWaitingroom() : new Location(Bukkit.getWorld("world"), 0, 200, 0));
		
		
	}
	
	public static void setMOTD(String motd) {
		DedicatedServer ss = (((CraftServer) Bukkit.getServer()).getHandle()
				.getServer());

		ss.setMotd(motd);
	}

}

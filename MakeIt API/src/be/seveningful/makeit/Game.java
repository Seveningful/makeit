package be.seveningful.makeit;


public interface Game {

	
	/*
	 * When server starts
	 */
	public void init();
	
	/*
	 * When mini-game starts
	 */
	public void start();
	
	/*
	 * When mini-game finishes
	 */
	public void finish();
	
	/*
	 * When server stops
	 */
	public void stop();
	
	/*
	 * Variables
	 */
	public GameState getGameState();
	

	
	
}

package be.seveningful.makeit;

import org.bukkit.plugin.PluginBase;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import be.seveningful.makeit.loops.MainLoop;
import be.seveningful.makeit.register.Register;

public class MakeIt extends JavaPlugin {

	static MakeIt instance;
	Game game;
	MainLoop mainloop;
	public static String name = "MAPI";
	
	Register register;
	private PluginBase plugin;
	public void onEnable() {
		
		instance = this;
		register = new Register();
		new BukkitRunnable() {
			
			@Override
			public void run() {
				
				while (game == null) {
					try {
						Thread.sleep(1000);
						System.out.println("[MIAPI] Waiting hook ...");
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
				}
				System.out.println("[MIAPI] Hooked with " + getGame() + " ...");
				game.init();
				
				cancel();
				
				
			}
		}.runTaskAsynchronously(this);
		
	}
	
	public void setGame(JavaPlugin plugin, Game game) {
		this.plugin = plugin;
		this.game = game;
	}
	public Register getRegister() {
		return register;
	}
	
	
	public static MakeIt getAPI() {
		return instance;
	}

	public Game getGame() {
		return game;
	}

	public PluginBase getGamePlugin() {
		return plugin;
	}

	public void runMainTask() {
		mainloop = new MainLoop();
		
	}
	
	public MainLoop getMainLoop() { return mainloop; }

}

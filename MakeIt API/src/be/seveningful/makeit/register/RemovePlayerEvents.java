package be.seveningful.makeit.register;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import be.seveningful.makeit.GameUtil;
import be.seveningful.makeit.MakeIt;

public class RemovePlayerEvents implements Listener {
	
	
	
	MakeIt makeit = MakeIt.getAPI();
	
	@EventHandler
	public void disconnect(PlayerQuitEvent e) {
		if(!makeit.getGame().getGameState().isAlive(e.getPlayer().getUniqueId())) return;
			if(makeit.getGame().getGameState().getConnection().canDisconnect()) {
			
			GameUtil.broadcast(ChatColor.RED + " " + e.getPlayer().getDisplayName() 
							+ ChatColor.WHITE 
							+ " vient de se déconnecter en pleine partie ! Il a " 
							+ (makeit.getGame().getGameState().getConnection().getRejoinSeconds()/60 == 0 
								? makeit.getGame().getGameState().getConnection().getRejoinSeconds() 
										+ " secondes "
									: makeit.getGame().getGameState().getConnection().getRejoinSeconds()/60
										+ " minutes ") 
							+ "pour se reconnecter !");
			makeit.getGame().getGameState().getConnection().addReconnectLoop(e.getPlayer().getUniqueId());
			
		} else {
			GameUtil.broadcast(ChatColor.RED + " " + e.getPlayer().getDisplayName() 
						+ ChatColor.WHITE 
						+ " vient de se déconnecter en pleine partie ! Il ne peut " 
						+ ChatColor.RED + ChatColor.BOLD.toString() 
						+ "plus" + ChatColor.WHITE 
						+ " se reconnecter !");
			makeit.getGame().getGameState().removePlayer(e.getPlayer().getUniqueId());
		}
	}
	
}

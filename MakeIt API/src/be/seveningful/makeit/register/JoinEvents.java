package be.seveningful.makeit.register;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import be.seveningful.makeit.GameState;
import be.seveningful.makeit.GameUtil;
import be.seveningful.makeit.MakeIt;
import be.seveningful.makeit.variables.GameProgress;

public class JoinEvents implements Listener {

	
	
	@EventHandler
	public void join(PlayerJoinEvent e) {
		GameState state = MakeIt.getAPI().getGame().getGameState();
		GameProgress progress = MakeIt.getAPI().getGame().getGameState().getGameProgress();
		Player p = e.getPlayer();
		if(progress.getActualProgressId() == 1) {
			
			p.setGameMode(GameMode.ADVENTURE);
			e.setJoinMessage(MakeIt.getAPI().getGame().getGameState().getGameNamePrefix() + " " 
						+ ChatColor.YELLOW +ChatColor.BOLD.toString() + p.getDisplayName() 
						+ ChatColor.WHITE + " a rejoint la partie ! "
						+ ChatColor.RED + ChatColor.BOLD.toString()
						+ "(" + Bukkit.getOnlinePlayers().size() + "/" + Bukkit.getMaxPlayers() + ")");
		}else if (progress.getPlayingIds().contains(
				progress.getActualProgressId())) {
			/*
			 * Checks
			 */
				/*
				 * Checking if player
				 * is on ReconnectionLoop
				 */
				/*
				 * If not 
				 * setting spectator Mode to Player
				 */
				if(state.isAlive(e.getPlayer().getUniqueId())) {
					
					GameUtil.broadcast(ChatColor.RED + ChatColor.BOLD.toString() + e.getPlayer().getDisplayName() + ChatColor.WHITE + " est revenu dans la partie !");
					e.setJoinMessage(null);
				}
				
				else {
					
					GameUtil.setSpectator(p);
					e.setJoinMessage(null);
				}
			
		}
	}
}

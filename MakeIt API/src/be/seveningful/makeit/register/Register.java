package be.seveningful.makeit.register;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;

import be.seveningful.makeit.MakeIt;
import be.seveningful.makeit.countdown.CountDown;

public class Register {

	public Register() {
		
		PluginManager pm = Bukkit.getPluginManager();
		
		pm.registerEvents(new RemovePlayerEvents(), MakeIt.getAPI());
		pm.registerEvents(new JoinEvents(), MakeIt.getAPI());
	}
	
	CountDown 					countdown;
	
	public void setCountdown(CountDown countdown) {
		this.countdown = countdown;
	}
	
	public CountDown getCountdown() {
		return countdown;
	}
}

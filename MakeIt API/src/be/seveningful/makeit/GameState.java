package be.seveningful.makeit;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import be.seveningful.makeit.variables.ConnectionVariables;
import be.seveningful.makeit.variables.GameProgress;
import be.seveningful.makeit.variables.LocationsVariables;

public class GameState {
	/**
	 * @author Seveningful
	 * This class is used to manage (almost) all of the variables 
	 * Accessed by the main class of the API only
	 */
	
	List<UUID> alive = new ArrayList<UUID>();
	
	ConnectionVariables connection;
	GameProgress progress;
	LocationsVariables locations;
	String gamename;
	
	public GameState() {

		connection = new ConnectionVariables();
		progress = new GameProgress();
		locations = new LocationsVariables();
	}
	
	
	/*
	 * Game name
	 */
	public String getGameName(){
		
		return this.gamename;
	}
	public void setGamename(String gamename) {
		System.out.println("[MIAPI] Defined game name as \"" + gamename + "\"" );
		this.gamename = gamename;
	}
	
	public String getGameNamePrefix() { return ChatColor.WHITE + "[" + getGameName() + ChatColor.WHITE + "]"; }
	
	
	/*
	 * Getting AlivePlayers
	 * 1. Getting ALL Alive Players
	 * 2. Getting ALL CONNECTED Alive Players
	 * 3. Getting ALL DISCONNECTED Alive Players
	 */
	public List<UUID> getAlivePlayers() {
		return alive;
	}
	
	public List<UUID> getAliveAndConnectedPlayers() {
		List<UUID> list = new ArrayList<UUID>();
		if(getAlivePlayers().size() == 0) return list;
		for(UUID id : getAlivePlayers()) {
			if(Bukkit.getPlayer(id) != null) list.add(id);
		}
		return list;
	}
	
	public List<UUID> getAliveAndDisconnectedPlayers() {
		List<UUID> list = new ArrayList<UUID>();
		if(getAlivePlayers().size() == 0) return list;
		for(UUID id : getAlivePlayers()) {
			if(Bukkit.getPlayer(id) == null) list.add(id);
		}
		return list;
	}
	
	public boolean isAlive(UUID id) { return alive.contains(id); }
	
	public void addPlayer(UUID id) {
		alive.add(id);
	}
	public void removePlayer(UUID id) {
		alive.remove(id);
	}
	
	/*
	 * Vaiables getters
	 */
	
	public ConnectionVariables getConnection() {
		return connection;
	}
	
	public GameProgress getGameProgress() {
		return progress;
	}
	public LocationsVariables getLocations() {
		return locations;
	}
	
	
}

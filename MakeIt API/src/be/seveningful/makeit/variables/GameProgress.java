package be.seveningful.makeit.variables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;

import be.seveningful.makeit.exceptions.ExceptionThrower;

public class GameProgress {

	Map<Integer, String> states = new HashMap<Integer, String>(); 
	List<Integer> playing = new ArrayList<Integer>();
	int progressid = 1;

	public GameProgress() {
		
		states.put(0, ChatColor.GOLD + "Génération");
		states.put(1, ChatColor.GOLD + "Redémarrage");
		states.put(2, ChatColor.GOLD + "Rejoindre");
		states.put(50, ChatColor.GRAY + "En jeu");
		states.put(100, ChatColor.RED + "Fin du jeu");

	}

	public String getMOTDbyProgressId(int id) {
		return states.get(id);
	}

	public String getActualMOTD() {
		return states.get(progressid);
	}

	public int getActualProgressId() { return progressid; }

	public void registerProgress(int id, String display, boolean playing) {
		if(states.containsKey(id)) {
			ExceptionThrower.thowException(this, "The ID " + id + " is already used as state ID", false);
		} else {
			states.put(id, display);
			if(playing) this.playing.add(id);
		}
	}
	
	public void setProgress(int id) {
		progressid = id;
	}
	
	public void replaceProgressId(int id, String display) {
		states.put(id, display);
	}

	public List<Integer> getPlayingIds() {
		return playing;
	}



}

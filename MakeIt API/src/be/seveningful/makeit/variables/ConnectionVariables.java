package be.seveningful.makeit.variables;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import be.seveningful.makeit.MakeIt;
import be.seveningful.makeit.loops.ReconnectionLoop;

public class ConnectionVariables {

	boolean disconnect = false;
	int rejoin = 5;
	
	List<ReconnectionLoop> loops = new ArrayList<ReconnectionLoop>();
	
	public boolean canDisconnect() {
		return disconnect;
	}
	public void setDisconnect(boolean disconnect) {
		this.disconnect = disconnect;
	}
	public int getRejoinSeconds() {
		return rejoin;
	}
	public void setRejoin(int rejoin) {
		this.rejoin = rejoin;
	}
	
	public void addReconnectLoop(UUID id) {
		loops.add(new ReconnectionLoop(id));
	}
	
	public boolean hasReconnectionLoop(UUID id) { 
		for(ReconnectionLoop loop : loops) {
			if(loop.getId() == id ) {
				return true;
			}
		}
		return false;
	}
	
	public ReconnectionLoop getReconnectionLoop( UUID id ) {
		for(ReconnectionLoop loop : loops) {
			if(loop.getId() == id ) {
				return loop;
			}
		}
		return null;
	}
	
	public void removeReconnectionLoop(UUID id) {
		if(loops.contains(id)){
			getReconnectionLoop(id).cancel();
			loops.remove(id);
		}
		
	}
	
	
	
	
}

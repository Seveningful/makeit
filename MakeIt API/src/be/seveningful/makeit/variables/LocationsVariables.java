package be.seveningful.makeit.variables;

import org.bukkit.Location;

public class LocationsVariables {

	Location waitingroom;
	
	public void setWaitingroom(Location waitingroom) {
		this.waitingroom = waitingroom;
	}
	
	public Location getWaitingroom() {
		return waitingroom;
	}
	
	
}

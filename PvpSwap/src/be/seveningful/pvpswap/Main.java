package be.seveningful.pvpswap;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import be.seveningful.makeit.Game;
import be.seveningful.makeit.GameState;
import be.seveningful.makeit.MakeIt;
import be.seveningful.makeit.countdown.CountDown;
import be.seveningful.pvpswap.managers.SpawnsManager;

public class Main extends JavaPlugin implements Game {

	Game game;
	SpawnsManager spawns;
	private GameState gamestate = new GameState();
	@Override
	public void onEnable() {

		MakeIt.getAPI().setGame(this,this);
		game = this;
		
		
		/*
		 * Spawns Register
		 */
		spawns = new SpawnsManager();
		spawns.registerSpawn(new Location(Bukkit.getWorld("world"), 10, 50, 10));
		spawns.registerSpawn(new Location(Bukkit.getWorld("world"), 30, 50, 10));
		spawns.registerSpawn(new Location(Bukkit.getWorld("world"), 40, 50, 20));
		spawns.registerSpawn(new Location(Bukkit.getWorld("world"), 40, 50, 40));
		spawns.registerSpawn(new Location(Bukkit.getWorld("world"), 10, 50, 30));
		
		MakeIt.getAPI().getRegister().setCountdown(new CountDown() {
			@Override
			public void run()  {
				setSeconds(20);
				setPlayers(1);

				super.run();
			}
			
			@Override
			public void onFinish() {
				super.onFinish();
				Bukkit.broadcastMessage(getGameState().getGameNamePrefix() + ChatColor.GREEN + " D�but de la partie !");
			}
		});

		super.onEnable();
	}

	@Override
	public void finish() {

	}

	@Override
	public void init() {
		this.getGameState().setGamename("PvpSwap");
		System.out.println("[PVPSWAP] Loaded " + this.getGameState().getGameName() + " game with MIAPI !");
		MakeIt.getAPI().getRegister().getCountdown().run();
		/*
		 * Definition of variables
		 */
			/*
			 * Connection Variables
			 */
			getGameState().getConnection().setDisconnect(true);
			getGameState().getConnection().setRejoin(10);
			getGameState().getGameProgress().registerProgress(3, ChatColor.WHITE + "Pr�paration", true);

	}

	@Override
	public void start() {

		/*
		 * Teleport Players
		 */
		List<Location> spawns = new ArrayList<Location>();
		spawns.addAll(this.spawns.getSpawns());
		
		for(UUID uuid : getGameState().getAliveAndConnectedPlayers()) {
			Player p = Bukkit.getPlayer(uuid);
			p.teleport(spawns.get(0));
			p.setGameMode(GameMode.SURVIVAL);
			spawns.remove(0);  
		}
		getGameState().getGameProgress().setProgress(3);
		
		
		
	}

	@Override
	public void stop() {
		
		

	}

	@Override
	public GameState getGameState() {
		
		return gamestate;
	}

}

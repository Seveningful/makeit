package be.seveningful.pvpswap.managers;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;

public class SpawnsManager {

	
	
	ArrayList<Location> spawns = new ArrayList<Location>();
	

	public void registerSpawn(Location loc)  { spawns.add(loc); }
	
	public List<Location> getSpawns() { return spawns; }
	
	
	
}
